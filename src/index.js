"use strict";
const $ = require("jquery");

window.jQuery = $

if ($) {
    require("bootstrap");
    require("owl.carousel");
    require("@fancyapps/fancybox/dist/jquery.fancybox");
    require("./js/main.js");
    require("./scss/require.scss");
}
