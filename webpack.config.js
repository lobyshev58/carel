const path=require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const webpack = require('webpack');


module.exports = {
    entry: { main: './src/index.js' },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'main.js'
    },
    module: {
      rules: [
        { test: /\.css$/,
           use: [
          MiniCssExtractPlugin.loader,
          'css-loader' 
          ] 
        }, 
        
          {test: /\.pug$/,
        loader: 'pug-loader'},
        {
          test: /\.js/,
            exclude: /(node_modules|bower_components)/,
            use: [{
                loader: 'babel-loader'}]
        },
          {
            test: /\.(eot|svg|ttf|woff|woff2)$/,
            use: [
                     {
                        loader: 'file-loader',
                        options: {
                          name: '[name].[ext]',
                          outputPath: 'fonts/'
                        }
                     },
                 ]
        },
        {
          test: /\.(png|jpe?g|gif)$/i,
          use: [
            {
              loader: 'file-loader',
              options: {
								name: '[name].[ext]',
							}
              },
              
            ]
          
        },
        {
            test: /\.(scss)$/,
            use: [
               'style-loader', // inject CSS to page
               MiniCssExtractPlugin.loader,
             {
              loader: 'css-loader', // translates CSS into CommonJS modules
            },
             {
              loader: 'sass-loader' // compiles SASS to CSS
            }]
          },
      ]
    },
    
    devServer: {
        stats: 'errors-only'
        },
    plugins: [
        new ExtractTextPlugin('style.css'),
        new CopyWebpackPlugin({
          patterns: [{
          from: './src/img',
          to: './img',
        }],
      }),
        new MiniCssExtractPlugin({
          filename: '[name].css',
          chunkFilename: '[id].css',
        }),
          new HtmlWebpackPlugin({
            inject: false,
            hash: true,
            template: './src/pug/index.pug',
            filename: './index.html'
          }),
          new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery'
          }),
    ]
};